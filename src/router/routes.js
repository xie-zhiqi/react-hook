
import Login from "../pages/Login/index"
import Page404 from "../pages/page404"
const routes = [
    {
        path: "/login",
        element:<Login></Login>
    },
    {
        path: "*",
        element:<Page404></Page404>
    }
]

export default routes;