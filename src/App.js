import React, { Fagement, Fragment } from 'react'
import { Button } from 'antd'
import './App.css'
import { Link, useRoutes } from 'react-router-dom'
import routesConfig from "./router/routes.js"
const App = () => {
  let Element = useRoutes(routesConfig)
  return (
    <Fragment>
      <Link to="/login">登入</Link>
      {Element}
    </Fragment>
  )
}

export default App;